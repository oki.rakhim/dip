# DIP



## Definisi

Pengenalan prinsip desain yang menerangkan:
- tight coupled to loose coupled classes
- programming to interface
- single responsibility principle
- inversion of control
- dependency inversion principle
- factory method
- dependency injection
- strategy pattern


## Struktur Kode

Struktur kode dibagi dalam beberapa folder dan dibuat berjenjang dengan prefix angka berurut untuk menunjukkan perubahan kode secara progresif.


## Problem Statement

Sebuah toko dapat menerima pembayaran dalam sejumlah uang. Alat pembayaran bisa beragam.