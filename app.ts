/* import Toko  from './src/domain/003dipandfactory/toko'


const operasi1 = new Toko1('PX')
operasi1.terimaBayar(4000)
operasi1.terimaBayar(1000)  
 */


/* import Toko  from './src/domain/004dipandfactoryrefactored/toko'


const operasi2 = new Toko('PX')
operasi2.terimaBayar(4000,'tunai')
operasi2.terimaBayar(10000,'ovo')   */
















/**/ 
import Toko from './src/domain/005dependencyinjection/toko'
import TokoService from './src/domain/005dependencyinjection/tokoservice'

const toko = new Toko('PX')
TokoService.terimaBayar( toko, 4000,'tunai')
TokoService.terimaBayar( toko, 10000,'ovo')  




