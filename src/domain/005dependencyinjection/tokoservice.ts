
import AlatBayarFactory from "./alatbayarfactory"
import Toko from "./toko"


class TokoService{

 
    static terimaBayar(toko:Toko, jumlah:number, alatBayar: string){
        toko.terimaBayar(jumlah,AlatBayarFactory.getAlatBayar(alatBayar) )
    }

   

}


export default TokoService