import AlatBayarFactory from './alatbayarfactory'
import Tunai from './tunai'

class Toko {
    nama: string
    alatBayar: Tunai

    constructor(nama:string){
        this.nama= nama
    }

    public terimaBayar(jumlah:number){
        this.alatBayar = AlatBayarFactory.getAlatBayar()
        this.alatBayar.bayar(jumlah)
    }
}

export default Toko