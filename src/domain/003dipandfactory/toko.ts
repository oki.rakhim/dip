import AlatBayarFactory from './alatbayarfactory'
import IAlatBayar from './ialatbayar'

class Toko {
    nama: string
    alatBayar: IAlatBayar

    constructor(nama:string){
        this.nama= nama
    }

    public terimaBayar(jumlah:number){
        this.alatBayar = AlatBayarFactory.getAlatBayar()
        this.alatBayar.bayar(jumlah)
    }
}

export default Toko