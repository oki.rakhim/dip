import Tunai from './tunai'
import IAlatBayar from './ialatbayar'

/* class AlatBayarFactory {

    static getAlatBayar(): Tunai{
    
        return new Tunai()
    }

} */

const AlatBayarFactory = {

    getAlatBayar(): IAlatBayar{
        return new Tunai() 
    },

}

export default AlatBayarFactory