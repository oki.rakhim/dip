import Tunai from './tunai'
import Ovo from './ovo'
import IAlatBayar from './ialatbayar'

/* class AlatBayarFactory {

    static getAlatBayar(): Tunai{
    
        return new Tunai()
    }

} */

const AlatBayarFactory = {

    getAlatBayar(providerBayar:string): IAlatBayar{
        if (providerBayar==='tunai')
            return new Tunai()
        if (providerBayar==='ovo')
            return new Ovo()    
       
        throw Error('Alat bayar tidak tersedia')   
    },

}

export default AlatBayarFactory