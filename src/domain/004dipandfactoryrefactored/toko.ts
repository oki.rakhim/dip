import AlatBayarFactory from './alatbayarfactory'
import IAlatBayar from './ialatbayar'

class Toko {
    nama: string
    alatBayar: IAlatBayar

    constructor(nama:string){
        this.nama= nama
    }

    public terimaBayar(jumlah:number, providerBayar: string){
        this.alatBayar = AlatBayarFactory.getAlatBayar(providerBayar)
        this.alatBayar.bayar(jumlah)
    }
}

export default Toko